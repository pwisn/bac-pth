---
title: "BAC-FNA Results"
author: "Piotr Wiśniewski"
date: 10.09.2021
---

```{r setup, include=FALSE,  warning=FALSE, message=FALSE}
knitr::opts_chunk$set(echo = TRUE)
source("~/bac-pth/code/libraries.R")
source("~/bac-pth/code/data-import.R", encoding = "UTF8")
source("~/bac-pth/code/functions.R")
options(scipen = 100)

 
theme_gtsummary_compact()
theme_gtsummary_language("en", big.mark = "")

Sys.setenv("RSTUDIO_VERSION" = '1.4.1725')
 
plik_z_danymi <- "~/bac-pth/data/baza_do_oblicz_07.08.2021.xlsx"
df <- wczytaj_dane_z_excela(plik_z_danymi)

```

## Patient characteristics


```{r}
df %>% 
  select(
    Age=wiek,
    Gender=plec, 
    `Serum Ca [mg/dl]`=wapn_sur, 
    `Serum Pi [mg/dl]`=fosf_sur, 
    `Serum iPTH [pg/ml]`=pth_sur, 
    `Location` = lokalizacja,
    `Largest dimension [cm]` = wym_max,
    `Volume [ml]` = objetosc,
    `Shape Index` = ksztalt,
  ) %>% 
  mutate(
    Gender = factor(Gender,labels = c("Female","Male")), 
    Location = fct_recode(Location, 
                            "left lower"="ld", 
                            "left upper"="lg", 
                            "right lower"="pd", 
                            "right upper"="pg", 
                            "upper mediastinum"="e"
                          )
  ) %>% 
  tbl_summary(missing = "no") %>%
  modify_caption("Clinical, biochemical and ultrasound characteristics of the patients with PHPT.") %>%
  as_gt() %>%
  gt::tab_source_note(gt::md("*Shape Index = shortest to longest diameter*"))

```

### By gender

```{r}

df %>% 
  select(
    Age=wiek,
    Gender=plec, 
    `Serum Ca [mg/dl]`=wapn_sur, 
    `Serum Pi [mg/dl]`=fosf_sur, 
    `Serum iPTH [pg/ml]`=pth_sur, 
    `Location` = lokalizacja,
    `Largest dimension [cm]` = wym_max,
    `Volume [ml]` = objetosc,
    `Shape Index` = ksztalt,
  ) %>% 
  mutate(
    Gender = factor(Gender,labels = c("Female","Male")), 
    Location = fct_recode(Location, 
                            "left lower"="ld", 
                            "left upper"="lg", 
                            "right lower"="pd", 
                            "right upper"="pg", 
                            "upper mediastinum"="e"
                          )
  ) %>% 
  tbl_summary(by=Gender, missing = "no") %>%
  add_p() %>%
  modify_caption("Patients' characteristics by gender.")

```

### by age caterogy

```{r}

df %>% 
  select(
    Age=wiek,
    Gender=plec, 
    `Serum Ca [mg/dl]`=wapn_sur, 
    `Serum Pi [mg/dl]`=fosf_sur, 
    `Serum iPTH [pg/ml]`=pth_sur, 
    `Location` = lokalizacja,
    `Largest dimension [cm]` = wym_max,
    `Volume [ml]` = objetosc,
    `Shape Index` = ksztalt,
  ) %>% 
  mutate(
    Gender = factor(Gender,labels = c("Female","Male")), 
    Age = cut(Age, breaks=c(0,61,100), include.lowest = T, right = F, labels = c("< 61 years",">= 61 years")),
    Location = fct_recode(Location, 
                            "left lower"="ld", 
                            "left upper"="lg", 
                            "right lower"="pd", 
                            "right upper"="pg", 
                            "upper mediastinum"="e"
                          )
  ) %>% 
  tbl_summary(by=Age, missing = "no") %>%
  add_p() %>%
  modify_caption("Patients' characteristics by age")

```

### by surgical treatment status

```{r}

df %>% 
  select(
    Age=wiek,
    Gender=plec, 
    `Serum Ca [mg/dl]`=wapn_sur, 
    `Serum Pi [mg/dl]`=fosf_sur, 
    `Serum iPTH [pg/ml]`=pth_sur, 
    `Location` = lokalizacja,
    `Largest dimension [cm]` = wym_max,
    `Volume [ml]` = objetosc,
    `Shape Index` = ksztalt,
    `MIBI retention` = test_status_mibi,
    `Surgery` = operowany,
  ) %>% 
  mutate(
    Surgery = factor(Surgery, labels = c("No","Yes")),
    Gender = factor(Gender,labels = c("Female","Male")), 
    Location = fct_recode(Location, 
                            "left lower"="ld", 
                            "left upper"="lg", 
                            "right lower"="pd", 
                            "right upper"="pg", 
                            "upper mediastinum"="e"
                          )
  ) %>% 
  tbl_summary(by=Surgery, missing = "no") %>%
  modify_spanning_header(c("stat_1", "stat_2") ~ "**Underwent parathyroidectomy**") %>%
  add_p() %>%
  modify_caption("Patients' characteristics by surgery status") 
  

```

## iPTH-WC results 

```{r}


df %>% 
  select(
    `Washout iPTH [pg/ml]`=pth_bac, 
    `Washout : serum iPTH ratio`=pth_index,
    `Diagnostic category` = falsz_prawda 
  ) %>% 
   mutate(
    `Diagnostic category` = fct_recode(`Diagnostic category`, 
                            "Negative"="FU", 
                            "Positive"="PD", 
                          )
  ) %>%
  tbl_summary(missing = "no")  
```

## Comparison of positive and negative results of iPTH-WC measurement.

```{r}
df %>% 
  select(
    Age=wiek,
    `Male sex`=plec, 
    `Serum Ca [mg/dl]`=wapn_sur, 
    `Serum Pi [mg/dl]`=fosf_sur, 
    `Serum iPTH [pg/ml]`=pth_sur, 
    `Diagnostic category` = falsz_prawda,
#    `Quality of Biopsy` =  qob_final,
    `Largest dimension [cm]` = wym_max,
    `Volume [ml]` = objetosc,
    `Shape Index` = ksztalt,
    `MIBI retention` = test_status_mibi,
    `Washout iPTH [pg/ml]`=pth_bac, 
    `Washout : serum iPTH ratio`=pth_index,
  ) %>% 
   mutate(
#    `Quality of Biopsy` = as.numeric(`Quality of Biopsy`),
    `Diagnostic category` = fct_recode(`Diagnostic category`, 
                            "Negative"="FU", 
                            "Positive"="PD", 
                          )
  ) %>%
  tbl_summary(by=`Diagnostic category`, missing = "no") %>%
  modify_spanning_header(c("stat_1", "stat_2") ~ "**iPTH-WC diagnostic category**") %>%
  add_p() %>%
  as_gt() %>%
  tab_footnote(
     footnote = "ratio of longest to shortest dimension",
     locations = cells_body(
     columns=c("label"), rows=8)
    )  %>%
  tab_footnote(
     footnote = "performed in 74 cases",
     locations = cells_body(
     columns=c("label"), rows=9)
    )
```

## Parathyroid volume before and after FNA.

```{r}
df %>% 
  select(lp,`Before FNA` = objetosc, `After FNA`= objetosc_post) %>%
  pivot_longer(cols=c(`Before FNA`,`After FNA`), names_to = "timepoint", values_to = "Volume [ml]") %>%
  filter(!is.na(`Volume [ml]`)) %>%
  group_by(lp) %>%
  filter(n()==2) %>%
  ungroup() %>%
  tbl_summary(
     by = "timepoint",
     include = -lp,
    missing = "no"
              ) %>%
  add_p(test = list(all_continuous() ~ "paired.wilcox.test"), group = lp) 
```


```{r fig1}
p1 <- ggplot(df, aes(x=log(pth_sur),y=log(pth_bac))) + geom_point() + theme_minimal()
p2 <- ggplot(df, aes(x= wapn_sur ,y=log(pth_bac))) + geom_point() + theme_minimal()
p3 <- ggplot(df, aes(x= log(objetosc) ,y=log(pth_bac))) + geom_point() + theme_minimal()
p4 <- ggplot(df, aes(x= ksztalt ,y=log(pth_bac))) + geom_point() + theme_minimal()

multiplot(p1,p2, p3, p4,  cols=2)

```

## Qualitative measures of P-FNAB performance

Measurement not performed in all patients

```{r}
df %>% 
  select(
      compliance,
      qob_final,
      powiklania,
 #   `Histopathology` = hist_pat
  ) %>% 
  tbl_summary(missing = "no",
    label = list(
      compliance ~ "Compliance Scale",
      qob_final ~ "Parathyroid Quality of Biopsy Scale",
      powiklania ~ "Safety Protocol Scale"
    )
  ) %>%
  add_n(col_label="**Pts assessed**") %>%
  modify_header(update = all_stat_cols() ~"**n (%)**") %>%
  modify_footnote(
    update = stat_0 ~ NA
  )
```

## iPTH-WC vs MIBI performance comparison

```{r}
df %>%
  tbl_cross(
    col = test_status_mibi,
    row = test_status_pth, 
    percent = "row",
    missing = "no",
    margin = NULL
  )
```

 proportion of overall agreement = `r 39/74`

 

 
